-- begin ATTENDANCESYSTEM_STUDENT
create table ATTENDANCESYSTEM_STUDENT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SURNAME varchar(255),
    NAME varchar(255),
    GROUP_ varchar(255),
    --
    primary key (ID)
)^
-- end ATTENDANCESYSTEM_STUDENT
-- begin ATTENDANCESYSTEM_ATTENDANCE_REPORT
create table ATTENDANCESYSTEM_ATTENDANCE_REPORT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    STUDENT_ID varchar(36),
    DAY_TASK_ID varchar(36),
    ATTEND_STATUS varchar(255),
    --
    primary key (ID)
)^
-- end ATTENDANCESYSTEM_ATTENDANCE_REPORT
-- begin ATTENDANCESYSTEM_DAY_TASK
create table ATTENDANCESYSTEM_DAY_TASK (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TASK_NAME varchar(255),
    DESCRIPTION varchar(255),
    TASK_TYPE varchar(255),
    DATE_ date,
    --
    primary key (ID)
)^
-- end ATTENDANCESYSTEM_DAY_TASK
