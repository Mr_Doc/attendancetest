package com.company.attendancesystem.web.screens.attendancereport;

import com.haulmont.cuba.gui.screen.*;
import com.company.attendancesystem.entity.AttendanceReport;

@UiController("attendancesystem_AttendanceReport.browse")
@UiDescriptor("attendance-report-browse.xml")
@LookupComponent("attendanceReportsTable")
@LoadDataBeforeShow
public class AttendanceReportBrowse extends StandardLookup<AttendanceReport> {
}