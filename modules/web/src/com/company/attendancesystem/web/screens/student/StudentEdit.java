package com.company.attendancesystem.web.screens.student;

import com.haulmont.cuba.gui.screen.*;
import com.company.attendancesystem.entity.Student;

@UiController("attendancesystem_Student.edit")
@UiDescriptor("student-edit.xml")
@EditedEntityContainer("studentDc")
@LoadDataBeforeShow
public class StudentEdit extends StandardEditor<Student> {
}