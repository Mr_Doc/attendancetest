package com.company.attendancesystem.web.screens;

import com.company.attendancesystem.entity.AttendanceReport;
import com.company.attendancesystem.entity.DayTask;
import com.company.attendancesystem.entity.Student;
import com.company.attendancesystem.web.screens.attendancereport.AttendanceReportBrowse;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.vaadin.ui.Notification;

import javax.inject.Inject;
import java.util.List;

@UiController("attendancesystem_AttendStatusScreen")
@UiDescriptor("attend-status-screen.xml")
public class AttendStatusScreen extends Screen {

    @Inject
    private CollectionLoader<AttendanceReport> attendanceReportsDl;
    @Inject
    private GroupTable<Student> studentsTable;
    @Inject
    private CollectionLoader<Student> studentsDl;
    @Inject
    private UiComponents uiComponents;
    @Inject
    private Label<String> label;
    @Inject
    private DataManager dataManager;
    private List<AttendanceReport> list;
    @Inject
    private Screens screens;

    @Subscribe
    public void onInit(InitEvent event) {
        studentsDl.load();
        attendanceReportsDl.load();
        initDayColumn();
    }

    public void setList(List<AttendanceReport> list) {
        this.list = list;
    }

    public void setDayTask(DayTask dayTask) {
        label.setValue(dayTask.getDate().toString());
    }

    protected void initDayColumn(){
        studentsTable.addGeneratedColumn("Статус",student -> {
            TextField textField = uiComponents.create(TextField.class);
            textField.addTextChangeListener(event->{
                Student selected = studentsTable.getSingleSelected();
                for (AttendanceReport object:list) {
                    if (object.getStudent().equals(selected)){
                        object.setAttendStatus(event.getText());
                        break;
                    }
                }
            });
            return textField;
        });
    }

    @Subscribe("button")
    public void onButtonClick(Button.ClickEvent event) {
        for (AttendanceReport report:list) {
            dataManager.commit(report);
        }
        screens.create(AttendanceReportBrowse.class).show();
        Notification.show("Создано");
    }

}