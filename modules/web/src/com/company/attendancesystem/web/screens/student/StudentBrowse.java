package com.company.attendancesystem.web.screens.student;

import com.haulmont.cuba.gui.screen.*;
import com.company.attendancesystem.entity.Student;

@UiController("attendancesystem_Student.browse")
@UiDescriptor("student-browse.xml")
@LookupComponent("studentsTable")
@LoadDataBeforeShow
public class StudentBrowse extends StandardLookup<Student> {
}