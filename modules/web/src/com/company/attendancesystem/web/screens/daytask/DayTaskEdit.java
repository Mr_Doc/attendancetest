package com.company.attendancesystem.web.screens.daytask;

import com.haulmont.cuba.gui.screen.*;
import com.company.attendancesystem.entity.DayTask;

@UiController("attendancesystem_DayTask.edit")
@UiDescriptor("day-task-edit.xml")
@EditedEntityContainer("dayTaskDc")
@LoadDataBeforeShow
public class DayTaskEdit extends StandardEditor<DayTask> {
}