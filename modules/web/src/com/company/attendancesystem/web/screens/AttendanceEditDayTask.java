package com.company.attendancesystem.web.screens;

import com.company.attendancesystem.entity.AttendanceReport;
import com.company.attendancesystem.entity.DayTask;
import com.company.attendancesystem.entity.Student;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@UiController("attendancesystem_AttendaceEditDaytask")
@UiDescriptor("attendance-edit-dayTask.xml")
public class AttendanceEditDayTask extends Screen {

    @Inject
    private DateField<LocalDate> dateFieldStartDate;
    @Inject
    private TextField<String> descriptionField;
    @Inject
    private TextField<String> taskNameField;
    @Inject
    private TextField<String> taskTypeField;
    @Inject
    private TwinColumn<Student> twinColumn;
    @Inject
    private Metadata metadata;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<Student> studentsDc;
    @Inject
    private CollectionLoader<Student> studentsDl;
    @Inject
    private Dialogs dialogs;
    @Inject
    private Screens screens;

    @Subscribe
    public void onInit(InitEvent event) {
        studentsDl.load();
        int i = 0;
    }



    @Subscribe("createButton")
    public void onCreateButtonClick(Button.ClickEvent event) {
        DayTask dayTask = metadata.create(DayTask.class);
        dayTask.setDate(dateFieldStartDate.getValue());
        dayTask.setTaskName(taskNameField.getValue());
        dayTask.setDescription(descriptionField.getRawValue());
        dayTask.setTaskType(taskTypeField.getRawValue());
        dataManager.commit(dayTask);
        Collection<Student> list = twinColumn.getValue();
        List<AttendanceReport> attendanceReportList = new ArrayList<>();
        for (Student object:list) {
            AttendanceReport report = metadata.create(AttendanceReport.class);
            report.setDayTask(dayTask);
            report.setStudent(object);
            attendanceReportList.add(report);
        }
        AttendStatusScreen statusScreen = screens.create(AttendStatusScreen.class);
        statusScreen.setDayTask(dayTask);
        statusScreen.setList(attendanceReportList);
        statusScreen.show();

    }
}