package com.company.attendancesystem.web.screens.daytask;

import com.haulmont.cuba.gui.screen.*;
import com.company.attendancesystem.entity.DayTask;

@UiController("attendancesystem_DayTask.browse")
@UiDescriptor("day-task-browse.xml")
@LookupComponent("dayTasksTable")
@LoadDataBeforeShow
public class DayTaskBrowse extends StandardLookup<DayTask> {
}