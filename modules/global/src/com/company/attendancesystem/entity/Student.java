package com.company.attendancesystem.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Table(name = "ATTENDANCESYSTEM_STUDENT")
@Entity(name = "attendancesystem_Student")
@NamePattern("%s %s|surname,name")
public class Student extends StandardEntity {
    private static final long serialVersionUID = 4605898853592835957L;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "NAME")
    private String name;

    @Column(name = "GROUP_")
    private String group;

    @OneToMany(mappedBy = "student")
    private List<AttendanceReport> attendance;

    public List<AttendanceReport> getAttendance() {
        return attendance;
    }

    public void setAttendance(List<AttendanceReport> attendance) {
        this.attendance = attendance;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return surname + " " + name;
    }
}