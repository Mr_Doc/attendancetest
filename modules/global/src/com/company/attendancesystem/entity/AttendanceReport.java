package com.company.attendancesystem.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;

@Table(name = "ATTENDANCESYSTEM_ATTENDANCE_REPORT")
@Entity(name = "attendancesystem_AttendanceReport")
public class AttendanceReport extends StandardEntity {
    private static final long serialVersionUID = -1031444230993994579L;

    @Lookup(type = LookupType.SCREEN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STUDENT_ID")
    private Student student;

    @Lookup(type = LookupType.SCREEN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DAY_TASK_ID")
    private DayTask dayTask;

    @Column(name = "ATTEND_STATUS")
    private String attendStatus;

    public String getAttendStatus() {
        return attendStatus;
    }

    public void setAttendStatus(String attendStatus) {
        this.attendStatus = attendStatus;
    }

    public DayTask getDayTask() {
        return dayTask;
    }

    public void setDayTask(DayTask dayTask) {
        this.dayTask = dayTask;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}